# `UtilitR` : Documentation `R` à l'usage des agents de l'Insee 

<!-- badges: start -->
[![pipeline status](https://gitlab.com/linogaliana/documentationR/badges/master/pipeline.svg)](https://gitlab.com/linogaliana/documentationR/-/commits/master)
<!-- badges: end -->

 
Code source pour le projet `UtilitR`, documentation collaborative `R` 
à l'usage des agents de l'Insee. Elle a vocation à être validée annuellement 
afin de produire un guide des bonnes pratiques et des fiches techniques.

Elle prend la forme d'un livre qui est pour l'instant déployé à l'adresse 
[suivante](https://linogaliana.gitlab.io/documentationR/index.html). 

Il est également possible de télécharger le fichier au format `pdf` 
ou les  fichiers `html` pour les consulter hors-connexion en cliquant
[ici](https://gitlab.com/linogaliana/documentationR/-/jobs/artifacts/master/download?job=buildbookdown)


# Objectif de la documentation

**Cette documentation s'adresse à tous les agents de l'Insee dans le cadre d'un usage courant de `R`.** 
Elle est conçue pour aider les agents à réaliser des traitements statistiques usuels avec `R` et à produire des sorties (graphiques, cartes, documents). Cette documentation présente succinctement les outils les plus adaptés à ces tâches, et oriente les agents vers les ressources documentaires pertinentes. En revanche, elle n'aborde pas les outils les plus avancés, notamment ceux utilisés dans un cadre de développement logiciel.

Cette documentation a pour ambition de répondre à trois questions générales:

* Comment travailler avec `R` à l'Insee?
* Quelles sont les bonnes pratiques à respecter pour bien utiliser `R`?
* Comment réaliser des tâches standards avec `R`?

Deux points importants sont à noter:

* **Cette documentation recommande les outils et les *packages* les plus adaptés au contexte d'utilisation de `R` à l'Insee**. Ces recommandations ne sont pas nécessairement adaptées à d'autres contextes, et pourront évoluer lorsque ce contexte évoluera.
* **Cette documentation recommande d'utiliser `R` avec `Rstudio`**, qui apparaît comme la solution la plus simple et la plus complète pour un usage courant de `R`.

# Comment contribuer à la documentation?

**Toutes les personnes volontaires peuvent contribuer à la documentation.** Un guide détaillé pour les contributeurs est disponible dans le fichier `CONTRIBUTING.md`.